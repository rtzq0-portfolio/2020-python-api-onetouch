#!/bin/bash
# Copyright 2020 Jason Ritzke
curl -H "Content-Type: application/json" --data-binary '@postbody.json' -v http://$1/builds
