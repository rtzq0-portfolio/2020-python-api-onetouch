#!/bin/bash
# Copyright 2020 Jason Ritzke
pushd AMI
packer build packer.json
popd
terragrunt apply --terragrunt-non-interactive --auto-approve
