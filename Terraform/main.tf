terraform {
  backend "s3" {}
}

provider "aws" {
  version = "2.46.0"
}

data "aws_ami" "api_server" {
  owners = ["self"]
  filter {
    name   = "name"
    values = ["ami-api-server-rtzq0"]
  }
}

resource "tls_private_key" "mgmt" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

output "ssh_private_key" {
  sensitive = true
  value = tls_private_key.mgmt.private_key_pem
}

resource "aws_key_pair" "mgmt" {
  key_name   = "api-server-mgmt"
  public_key = tls_private_key.mgmt.public_key_openssh
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.24.0"
  name = "api-server"
  cidr = "10.0.0.0/16"
  azs             = ["us-west-2a", "us-west-2b"]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]
}

module "security_group_alb" {
  source  = "terraform-aws-modules/security-group/aws//modules/http-80"
  version = "3.4.0"
  name        = "api-server-alb-sg"
  description = "Security group to isolate ALB"
  vpc_id      = module.vpc.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "security_group_ec2" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.4.0"
  name        = "api-server-ec2-sg"
  description = "Security group to isolate ec2 instance"
  vpc_id      = module.vpc.vpc_id
  computed_ingress_with_source_security_group_id = [
    {
      from_port                = 8000
      to_port                  = 8000
      protocol                 = "tcp"
      description              = "http-alt"
      source_security_group_id = module.security_group_alb.this_security_group_id
    }
  ]
  number_of_computed_ingress_with_source_security_group_id = 1
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "5.0.0"
  name_prefix = "apisvr"
  subnets = module.vpc.public_subnets
  vpc_id = module.vpc.vpc_id
  load_balancer_type = "application"
  security_groups    = [module.security_group_alb.this_security_group_id]
  target_groups = [
    {
      name_prefix      = "webalt"
      backend_protocol = "HTTP"
      backend_port     = 8000
      target_type      = "instance"
    }
  ]
  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]
}

output "alb_dns_name" {
  value = module.alb.this_lb_dns_name
}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.12.0"
  key_name = aws_key_pair.mgmt.key_name
  associate_public_ip_address = true
  instance_type = "t2.micro"
  instance_count = 1
  name          = "api-server-rtzq0"
  ami           = data.aws_ami.api_server.id
  subnet_id     = module.vpc.private_subnets[0]
  vpc_security_group_ids      = [module.security_group_ec2.this_security_group_id]
  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 10
    },
  ]
}

resource "aws_lb_target_group_attachment" "api_server" {
  target_group_arn = module.alb.target_group_arns[0]
  target_id        = module.ec2_instance.id[0]
  port             = 8000
}
