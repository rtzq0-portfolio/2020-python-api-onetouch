remote_state {
  backend = "s3"
  config = {
    bucket = get_env("TF_VAR_tfstate_bucket", false)
    region = get_env("AWS_DEFAULT_REGION", false)
    encrypt = true
    key = "terraform.tfstate"
    dynamodb_table = get_env("TF_VAR_tfstate_dynamodb_table", false)
  }
}

terraform {
  source = "Terraform"
}
