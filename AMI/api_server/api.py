#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""API Server

This server responds to a certain post request with a response providing the
latest entry meeting a few qualifiers. This API is written with hug and is
intended to be deployed via gunicorn.
"""

__author__ = "Jason Ritzke"
__copyright__ = "Copyright 2020, Jason Ritzke"
__credits__ = ["Jason Ritzke"]
__license__ = "All rights reserved"
__version__ = "0.1.0"
__maintainer__ = "Jason Ritzke"
__email__ = "rtzq0-careers@pwn.nz"
__status__ = "Production"

import hug
from marshmallow import Schema, fields, validate

class build_schema(Schema):
    """Schema for an individual build in the posted object
    """
    runtime_seconds = fields.Int(required=True)
    build_date = fields.Int(required=True)
    result = fields.Str(validate=validate.OneOf(('SUCCESS', 'FAILURE')))
    output = fields.Str()

class buildlist_schema(Schema):
    """Schema for the list of all builds
    """
    Builds = fields.List(fields.Nested(build_schema, required=True))

class joblist_schema(Schema):
    """Schema for the list of all jobs
    """
    jobs = fields.Dict(keys=fields.Str(), values=fields.Nested(buildlist_schema, required=True))

@hug.post('/builds')
def latest_build(body: joblist_schema()):
    """Get the latest successful build from a submitted job list

    Args:
        body (:obj:`build_schema): Required post body

    Returns:
        str: POST response containing build information
    """
    latest_build_date = 0
    latest_build_output = ""
    joblist = body["jobs"]
    for job in joblist.values():
        build_list = job["Builds"]
        for build in build_list:
            if build.get("result") == 'SUCCESS':
                if build.get("build_date") > latest_build_date:
                    latest_build_date = build["build_date"]
                    latest_build_output = build["output"]
    ami_id = latest_build_output.split(' ')[2]
    commit_hash = ami_id.split('-')[1]
    return {
            "latest": {
                "build_date": latest_build_date,
                "ami_id": ami_id,
                "commit_hash": commit_hash
                }
            }
