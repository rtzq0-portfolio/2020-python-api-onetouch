#!/bin/bash
# Copyright 2020 Jason Ritzke
export PIPENV_VENV_IN_PROJECT="enabled"
pipenv install
