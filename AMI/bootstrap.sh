#!/bin/bash
# Copyright 2020 Jason Ritzke

# Install Pipenv
apt-get update
apt-get install -y pipenv virtualenv python3.8

# Create new user
useradd --no-create-home --user-group --shell /bin/false -c "API Server" --system apiserver

# Move files
mv /tmp/api_server /opt/api_server
chown --recursive apiserver:apiserver /opt/api_server
chmod --recursive 0755 /opt/api_server
mv /opt/api_server/api_server.service /etc/systemd/system/api_server.service
chown root:root /etc/systemd/system/api_server.service
chmod 0755 /etc/systemd/system/api_server.service

# Install dependencies as user
pushd /opt/api_server
sudo --user apiserver --group apiserver ./pipenv_setup.sh
pushd

# Start service
systemctl daemon-reload
systemctl enable api_server.service
