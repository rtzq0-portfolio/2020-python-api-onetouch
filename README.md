# API server onetouch deployment

## Requirements

The following must be in your `PATH`

- `Terraform`
- `Terragrunt`
- `Packer`

## Usage

Set the following environment variables

```
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_DEFAULT_REGION
TF_VAR_tfstate_bucket
TF_VAR_tfstate_dynamodb_table
```

Run `one_touch.sh`

## Testing

`cd` into `Tests`, then execute `post_sample.sh` with the DNS name of the ALB
as the first argument. 

## SSH Key

In order to retrieve the EC2 SSH instance key, use `terragrunt show`
